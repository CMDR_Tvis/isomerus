package io.github.commandertvis.isomerus.api

/**
 * Represents an element of chemical compound that can accept and give electrons away. Designed to use in [MutableList]
 */
interface Valenceable {
    /**
     * Chemical bonds in list
     */
    var bonds: MutableList<Int>
    /**
     * ID in list
     */
    val id: Int
    /**
     * Number in a molecule
     */
    var numberInMolecule: Int
    /**
     * Valence
     */
    val valence: Int

    /**
     * Represents an operation of binding with an other [Valenceable]
     * @param other [Valenceable] to bind with
     * @return was the binding successful
     */
    fun bind(other: Valenceable): Boolean

    /**
     * Returns true if this [Valenceable] can bind
     * @return can this [Valenceable] bind currently
     */
    fun canBind(): Boolean
}
