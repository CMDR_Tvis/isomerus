package io.github.commandertvis.isomerus.api

/**
 * Converts list of [Atom] to readable formula that represents chemical bonds
 * @return string of atoms of the list and their bonds
 */
fun List<Atom>.asFormulaString() = buildString {
    this@asFormulaString.forEach {
        append("${it.type.symbol}[${buildString {
            it.bonds.sorted().forEach { bond ->
                if (isNotEmpty())
                    append(",")

                append(bond)
            }
        }}]")
    }
}

internal fun List<List<Atom>>.containsDeep(list: List<Atom>): Boolean {
    val string = list.asFormulaString()

    forEach {
        if (it.asFormulaString() == string)
            return true
    }

    return false
}
