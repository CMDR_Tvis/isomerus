package io.github.commandertvis.isomerus.api

/**
 * Represents an atom
 */
data class Atom(
    override val id: Int,
    /**
     * Type of this atom
     */
    val type: AtomType
) : Valenceable {
    override var numberInMolecule = 0
    override var bonds: MutableList<Int> = mutableListOf()
    override val valence
        get() = type.valence

    override fun bind(other: Valenceable) =
        if (other.bonds.size + 1 <= other.valence && bonds.size + 1 <= valence) {
            other.bonds.add(id)
            bonds.add(other.id)
            true
        } else
            false

    override fun canBind() = valence - bonds.size > 0

    /**
     * Copies this class deeply
     * @return deep copy of this object
     */
    fun copy() = copy(id = id, type = type).also {
        it.numberInMolecule = this@Atom.numberInMolecule
        it.bonds = mutableListOf<Int>().apply { addAll(this@Atom.bonds) }
    }
}
