package io.github.commandertvis.isomerus.api

/**
 * Represents a group of unbound atoms
 */
data class AtomStack(
    /**
     * Type of stack
     */
    val type: AtomType,
    /**
     * Quantity of atoms in the stack
     */
    var quantity: Int
) {
    companion object {
        /**
         * Builds a list of [AtomStack] from a formula like "C2H6O"
         */
        @JvmStatic
        fun fromString(formula: String): List<AtomStack>? {
            formula.run {
                var string = this

                return mutableListOf<AtomStack>().apply {
                    while (true) {
                        AtomType.values().forEach { type ->
                            if (string.startsWith(type.symbol)) {
                                string = string.removePrefix(type.symbol)

                                var index = ""

                                run loop@{
                                    string.forEach {
                                        if (it.isDigit())
                                            index += it
                                        if (it.isLetter())
                                            return@loop
                                    }
                                }

                                string = string.removePrefix(index)

                                val indexInteger = if (index == "")
                                    1
                                else
                                    index.toIntOrNull() ?: return null

                                add(AtomStack(type, indexInteger))
                            }
                        }

                        if (string.isEmpty())
                            break
                    }
                }
            }
        }
    }
}
