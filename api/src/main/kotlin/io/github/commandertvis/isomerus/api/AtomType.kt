package io.github.commandertvis.isomerus.api

/**
 * Represents atom types
 */
enum class AtomType(
    /**
     * Valence of this atom to use
     */
    val valence: Int,
    /**
     * Symbol of this atom
     */
    val symbol: String
) {
    /**
     * Carbon atom
     */
    CARBON(4, "C"),
    /**
     * Hydrogen atom
     */
    HYDROGEN(1, "H"),
    /**
     * Oxygen atom
     */
    OXYGEN(2, "O");
}
