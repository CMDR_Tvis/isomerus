package io.github.commandertvis.isomerus.api

internal inline fun repeat(from: Int, to: Int, step: Int = 1, action: (i: Int) -> Unit) {
    var i = from
    while (i < to) {
        action(i)
        i += step
    }
}
