package io.github.commandertvis.isomerus.api

data class IsomerusOutput(
    val initialFormula: String,
    val results: List<List<Atom>>
)
