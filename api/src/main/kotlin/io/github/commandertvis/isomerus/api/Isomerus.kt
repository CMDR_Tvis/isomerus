package io.github.commandertvis.isomerus.api

/**
 * Represents a counter of isomers
 */
class Isomerus {
    private val founds: MutableList<List<Atom>> = mutableListOf()
    private val nodes: MutableList<MutableList<String>> = mutableListOf()
//    private val log: MutableMap<String, String> = mutableMapOf()

    /**
     * This function processes through a list of [AtomStack] to output available isomers
     * @param formula brutto-formula to convert
     */
    fun convert(formula: String): IsomerusOutput? {
        val initialAtoms = mutableListOf<Atom>()
        var id = 0

        (AtomStack.fromString(formula) ?: return null).forEach { stack: AtomStack ->
            repeat(stack.quantity) {
                initialAtoms += Atom(id, stack.type)
                id++
            }
        }

        initialAtoms[0].numberInMolecule = 1
        iterateBindings(initialAtoms, 1)

        return IsomerusOutput(/*log, */formula, founds)
    }

    private fun iterateBindings(
        atoms: List<Atom> = mutableListOf(),
        level: Int
    ): Boolean {
//        logRecorder?.invoke("Level - $level")
        nodes.run {
            val index = level - 1

            if (getOrNull(index) != null && atoms.asFormulaString() in get(index))
                return false
            else
                if (getOrNull(index) == null)
                    add(index, mutableListOf(atoms.asFormulaString()))
                else
                    get(index) += atoms.asFormulaString()
        }

        var doContinue = false

        atoms.forEach {
            if (it.canBind())
                doContinue = true
        }

        if (!doContinue) {
            if (!founds.containsDeep(atoms))
                founds += atoms

            return true
        }

        repeat(0, atoms.size) first@{ i ->
            val atom1 = atoms[i]

            if (atom1.numberInMolecule == 0 || !atom1.canBind())
                return@first

            repeat(0, atoms.size) second@{ j ->
                val atom2 = atoms[j]

                if (!atom2.canBind() || i == j)
                    return@second

                val atomsCopy = atoms.map(Atom::copy)

                if (atomsCopy[atom1.id].bind(atomsCopy[atom2.id])) {
                    atomsCopy[atoms.indexOf(atom2)].numberInMolecule = level + 1
//                    logRecorder?.invoke(atomsCopy.asFormulaString())
                    iterateBindings(atomsCopy, level + 1)
                }


            }
        }

        return false
    }
}
