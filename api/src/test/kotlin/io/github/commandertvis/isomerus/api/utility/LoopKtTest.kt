package io.github.commandertvis.isomerus.api.utility

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class LoopKtTest {
    @Test
    fun repeat() {
        var j = 0
        io.github.commandertvis.isomerus.api.repeat(0, 10) { i ->
            assertEquals(i, j)
            j++
        }
    }
}
