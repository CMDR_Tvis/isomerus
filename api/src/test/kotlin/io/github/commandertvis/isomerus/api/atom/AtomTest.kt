package io.github.commandertvis.isomerus.api.atom

import io.github.commandertvis.isomerus.api.Atom
import io.github.commandertvis.isomerus.api.AtomType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class AtomTest {
    @Test
    fun bind() {
        val atoms: List<Atom> = listOf(
            Atom(
                0,
                AtomType.HYDROGEN
            ),
            Atom(
                1,
                AtomType.HYDROGEN
            )
        )

        assertTrue(atoms[0].bind(atoms[1]))
        assertFalse(atoms[0].bind(atoms[0]))

        assertEquals(1, atoms[0].bonds[0])
        assertEquals(0, atoms[1].bonds[0])
    }

    @Test
    fun canBind() {
        assertTrue { Atom(
            0,
            AtomType.HYDROGEN
        ).canBind() }
    }

    @Test
    fun copy() = Atom(
        0,
        AtomType.HYDROGEN
    ).run { assertEquals(this, copy()) }
}
