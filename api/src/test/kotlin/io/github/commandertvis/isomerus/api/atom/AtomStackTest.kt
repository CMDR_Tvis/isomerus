package io.github.commandertvis.isomerus.api.atom

import io.github.commandertvis.isomerus.api.AtomStack
import io.github.commandertvis.isomerus.api.AtomType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class AtomStackTest {
    @Test
    fun fromString(): Unit =
        assertEquals(
            listOf(
                AtomStack(
                    AtomType.CARBON,
                    2
                ),
                AtomStack(
                    AtomType.HYDROGEN,
                    6
                ),
                AtomStack(
                    AtomType.OXYGEN,
                    1
                )
            ), AtomStack.fromString("C2H6O")
        )
}
