//package io.github.commandertvis.isomerus.api
//
//import io.github.commandertvis.isomerus.api.AtomStack
//import io.github.commandertvis.isomerus.api.asFormulaString
//import org.junit.jupiter.api.Test
//import kotlin.test.assertEquals
//
//internal class IsomerusTest {
//
//    @Test
//    fun convert(): Unit =
//        assertEquals(
//            "H[2]H[2]O[0,1]",
//            Isomerus().convert(AtomStack.fromString("H2O")!!)[0].asFormulaString()
//        )
//
//}
