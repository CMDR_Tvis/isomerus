package io.github.commandertvis.isomerus.api.utility

import io.github.commandertvis.isomerus.api.Atom
import io.github.commandertvis.isomerus.api.AtomType
import io.github.commandertvis.isomerus.api.asFormulaString
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ListExtensionsKtTest {
    @Test
    fun asFormulaString() {
        assertEquals(
            "H[2]H[2]O[0,1]",
            listOf(
                Atom(
                    0,
                    AtomType.HYDROGEN
                ).apply { bonds = mutableListOf(2) },
                Atom(
                    1,
                    AtomType.HYDROGEN
                ).apply { bonds = mutableListOf(2) },
                Atom(
                    2,
                    AtomType.OXYGEN
                ).apply { bonds = mutableListOf(0, 1) }
            ).asFormulaString()
        )
    }
}
