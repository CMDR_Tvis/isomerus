plugins { `maven-publish` }

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testImplementation(kotlin("test"))
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.2")
}

val project = this

tasks {
    create<Jar>("sourcesJar") {
        from(sourceSets.main.get().allSource)
        archiveClassifier.set("sources")
    }

    publish { dependsOn(build) }

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(project.components["java"])
                artifact(this@tasks["sourcesJar"])
                pom {
                    operator fun <T> Property<T>.invoke(value: T): Unit = set(value)

                    operator fun <T> SetProperty<T>.invoke(vararg value: T): Unit = set(value.toList())

                    packaging = "jar"
                    name(project.name)
                    description(project.description)
                    url("https://gitlab.com/CMDR_Tvis/isomerus")
                    inceptionYear("2018")

                    licenses {
                        license {
                            comments("Open-source license")
                            distribution("repo")
                            name("MIT")
                            url("https://gitlab.com/CMDR_Tvis/isomerus/blob/master/LICENSE")
                        }
                    }

                    developers {
                        developer {
                            email("postovalovya@gmail.com")
                            id("CMDR_Tvis")
                            name("Commander Tvis")
                            roles("architect", "developer")
                            timezone("Russian Federation/Novosibirsk")
                            url("https://commandertvis.github.io/")
                        }
                    }
                }
            }
        }
        repositories {
            maven("https://gitlab.com/api/v4/projects/9977646/packages/maven") {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }

                authentication { register("header", HttpHeaderAuthentication::class) }
            }
        }
    }

    test(Test::useJUnitPlatform)
}
