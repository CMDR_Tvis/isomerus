# Package io.github.commandertvis.isomerus.api
More generalized classes in API
# Package io.github.commandertvis.isomerus.api.atom
Atom storage related types
# Package io.github.commandertvis.isomerus.api.utility
Utility functions and extensions
