# Isomerus [![Pipeline status](https://gitlab.com/tvis/plugin-api/badges/master/pipeline.svg)](https://gitlab.com/tvis/plugin-api/commits/master)

Counts chemical isomers by brutto-formula. 

## Usage

### CLI

Download latest `publish-cli` artifact from [pipelines](https://gitlab.com/CMDR_Tvis/isomerus/pipelines), unzip and run launch script. 

### API

Include the API and call as you need. 

#### Maven

```xml
<depedencies>
    <dependency>
        <artifactId>api</artifactId>
        <groupId>io.github.commandertvis.isomerus</groupId>
        <scope>compile</scope>
        <version>0.1</version> 
  </dependency>
</depedencies>

<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/9977646/packages/maven</url>
    </repository>
</repositories>
```

#### Gradle

**Groovy DSL**: 

```gradle
repositories {
    maven { url 'https://gitlab.com/api/v4/projects/9977646/packages/maven' }
}

dependencies {
    implementation 'io.github.commandertvis.isomerus:api:0.1'
}
```

**Kotlin DSL**: 

```kotlin
repositories {
    maven("https://gitlab.com/api/v4/projects/9977646/packages/maven")
}

dependencies {
    implementation("io.github.commandertvis.isomerus:api:0.1")
}
```

#### JAR artifact

Download latest `publish-api` artifact from [pipelines](https://gitlab.com/tvis/config/pipelines) and add it to your classpath. 

## Docs

Documentation is published [here](http://cmdr_tvis.gitlab.io/isomerus/main/). 
