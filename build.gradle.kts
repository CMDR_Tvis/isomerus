import org.jetbrains.dokka.gradle.PackageOptions
import org.jetbrains.dokka.gradle.SourceRoot
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.dokka") version "0.9.17"
    kotlin("jvm") version "1.3.40"
}

allprojects {
    apply(plugin = "kotlin")

    description = "Isomers counting software"
    group = "io.github.commandertvis.isomerus"
    version = "0.1"

    repositories(RepositoryHandler::jcenter)
    dependencies { implementation(kotlin("stdlib-jdk8")) }

    tasks {
        fun KotlinCompile.setup() {
            kotlinOptions {
                apiVersion = "1.3"
                jvmTarget = "1.8"
                languageVersion = "1.3"
            }
        }

        compileKotlin(KotlinCompile::setup)
        compileTestKotlin(KotlinCompile::setup)
    }
}

tasks.dokka {
    impliedPlatforms = mutableListOf("JVM")
    includes = listOf(file("api/PACKAGES.md"))
    moduleName = "main"
    noStdlibLink = false
    outputDirectory = "public/"
    outputFormat = "html"

    perPackageOptions = mutableListOf(
        PackageOptions().apply {
            includeNonPublic = false
            prefix = "kotlin"
            reportUndocumented = true
            skipDeprecated = false
            suppress = false
        }
    )

    sourceRoots = mutableListOf(SourceRoot().apply { path = "api/src/main/kotlin/" })
}
