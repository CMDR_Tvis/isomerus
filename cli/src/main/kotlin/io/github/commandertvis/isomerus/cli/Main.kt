package io.github.commandertvis.isomerus.cli

import io.github.commandertvis.isomerus.api.Isomerus
import io.github.commandertvis.isomerus.api.asFormulaString

fun main(args: Array<String>) {
    Isomerus()
        .convert(args.getOrNull(0) ?: return)
        ?.results
        ?.forEach { println(it.asFormulaString()) }
}
