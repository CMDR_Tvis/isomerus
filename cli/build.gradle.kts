plugins { application }

dependencies {
    implementation(project(":api"))
    implementation(kotlin("stdlib-jdk8"))
}

application {
    applicationName = project.parent!!.name
    mainClassName = "io.github.commandertvis.isomerus.cli.MainKt"
}
