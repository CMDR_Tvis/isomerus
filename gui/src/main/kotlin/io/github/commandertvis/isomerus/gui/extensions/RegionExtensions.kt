package io.github.commandertvis.isomerus.gui.extensions

import io.github.commandertvis.isomerus.gui.BACKGROUND_SECONDARY
import javafx.scene.layout.Region
import tornadofx.paddingAll
import tornadofx.vboxConstraints

fun Region.commonLayout() {
    background = BACKGROUND_SECONDARY
    vboxConstraints {
        marginBottom = 10.0
        marginLeft = 20.0
        marginTop = 10.0
        paddingAll = 10
    }
}
