package io.github.commandertvis.isomerus.gui

import io.github.commandertvis.isomerus.gui.extensions.setupFont
import tornadofx.Stylesheet

class MainStyles : Stylesheet() {
    init {
        button { setupFont(Fonts.SANS_SERIF) }
        label { setupFont(Fonts.SANS_SERIF) }
        textField { setupFont(Fonts.SANS_SERIF) }
        textArea { setupFont(Fonts.MONOSPACED) }
    }
}
