package io.github.commandertvis.isomerus.gui

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXTextArea
import com.jfoenix.controls.JFXTextField
import io.github.commandertvis.isomerus.api.Isomerus
import io.github.commandertvis.isomerus.api.IsomerusOutput
import io.github.commandertvis.isomerus.api.asFormulaString
import io.github.commandertvis.isomerus.gui.extensions.commonLayout
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.ContextMenu
import javafx.scene.input.Clipboard
import javafx.scene.input.DataFormat
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import tornadofx.*
import java.io.File
import java.nio.charset.Charset
import kotlin.system.exitProcess

class MainView : View(APP_NAME) {
    override val root = BorderPane()
    private lateinit var inputField: JFXTextField
    private lateinit var countButton: JFXButton
    private lateinit var logArea: JFXTextArea
    private lateinit var saveOutputButton: JFXButton

    private var isomerusOutput: IsomerusOutput? = null
        set(value) {
            saveOutputButton.isDisable = value == null
            field = value
        }

    init {
        root.background = BACKGROUND_PRIMARY

        (currentStage as Stage).run {
            width = STAGE_WIDTH
            height = STAGE_HEIGHT
            isResizable = false

            root.left = vbox {
                onCloseRequest = EventHandler { exitProcess(0) }

                this@vbox.maxWidth = RIGHT_BOX_WIDTH
                this@vbox.minHeight = RIGHT_BOX_WIDTH

                label(APP_NAME) {
                    commonLayout()
                    alignment = Pos.CENTER
                }

                inputField = JFXTextField().attachTo(this) {
                    alignment = Pos.CENTER
                    commonLayout()
                }

                logArea = JFXTextArea().attachTo(this) {
                    isEditable = false
                    this@attachTo.minHeightProperty().set(LOG_HEIGHT)
                    commonLayout()
                    contextMenu = ContextMenu()
                    onMouseClicked = EventHandler {

                        if (text.isEmpty())
                            return@EventHandler

                        Clipboard.getSystemClipboard().setContent { this@setContent[DataFormat.PLAIN_TEXT] = text }
                    }
                }

                countButton = JFXButton("Count").attachTo(this) {
                    commonLayout()
                    buttonType = BUTTON_TYPE
                    minWidthProperty().set(BUTTON_WIDTH)

                    onMouseClicked = EventHandler {
                        logArea.text = ""

                        var animation = true

                        GlobalScope.launch(Dispatchers.JavaFx) {
                            isDisable = true

                            while (animation) {
                                delay(BUTTON_ANIMATION_DELAY)
                                this@attachTo.text = "Counting... "
                                delay(BUTTON_ANIMATION_DELAY)
                                this@attachTo.text = "Counting.. "
                                delay(BUTTON_ANIMATION_DELAY)
                                this@attachTo.text = "Counting. "
                            }

                            this@attachTo.text = "Count"
                            isDisable = false
                        }

                        GlobalScope.launch(Dispatchers.JavaFx) {
                            logArea.text = buildString {
                                if (inputField.text.isNotEmpty()) {
                                    val output = Isomerus().convert(inputField.text) ?: return@launch
                                    output.results.forEach { append("${it.asFormulaString()}\n\n") }
                                    isomerusOutput = output
                                }

                                animation = false
                            }
                        }
                    }
                }

                saveOutputButton = JFXButton("Save logs").attachTo(this) {
                    commonLayout()
                    buttonType = BUTTON_TYPE
                    isDisable = true
                    minWidthProperty().set(BUTTON_WIDTH)

                    onMouseClicked = EventHandler {
                        val file: File? = CHOOSER_SAVE_OUTPUT.showSaveDialog(this@run)

                        if (file != null) {
                            file.createNewFile()
                            file.writeText("${GSON.toJson(isomerusOutput)}\n", Charset.defaultCharset())
                        }
                    }
                }
            }

            root.right = vbox {
                //                text("vam bam")
//                canvas {
//                    heightProperty().set(100.0)
//                    widthProperty().set(100.0)
//                    this@canvas.graphicsContext2D.run {
//                        fill = CANVAS_COLOR
//                        fillRule = FillRule.EVEN_ODD
//
//                    }
//                }
            }
        }
    }
}
