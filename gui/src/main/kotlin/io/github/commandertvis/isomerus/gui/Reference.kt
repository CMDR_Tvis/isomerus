package io.github.commandertvis.isomerus.gui

import com.google.gson.GsonBuilder
import com.jfoenix.controls.JFXButton
import javafx.geometry.Insets
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontSmoothingType
import javafx.stage.FileChooser
import tornadofx.loadFont
import java.util.*

const val APP_NAME = "Isomerus"
const val BUTTON_ANIMATION_DELAY = 500L
const val BUTTON_WIDTH = 150.0
const val RIGHT_BOX_WIDTH = 400.0
const val LOG_HEIGHT = 300.0
const val STAGE_HEIGHT = 650.0
const val STAGE_WIDTH = 1000.0
inline val BACKGROUND_PRIMARY
    get() = Background(BackgroundFill(COLOR_PRIMARY, CornerRadii.EMPTY, Insets.EMPTY))
inline val BACKGROUND_SECONDARY
    get() = Background(BackgroundFill(COLOR_SECONDARY, CornerRadii.EMPTY, Insets.EMPTY))
inline val BUTTON_TYPE
    get() = JFXButton.ButtonType.FLAT
inline val COLOR_PRIMARY
    get() = Color.DARKGRAY!!
inline val COLOR_SECONDARY
    get() = Color.LIGHTGRAY!!
inline val CANVAS_COLOR
    get() = Color.WHITE!!
inline val FONT_SMOOTHING
    get() = FontSmoothingType.LCD
val CHOOSER_SAVE_OUTPUT by lazy {
    FileChooser().apply {
        extensionFilters.add(FileChooser.ExtensionFilter("JSON", "json"))
        initialFileName = "output.json"
        title = "Save output"
    }
}
val GSON by lazy { GsonBuilder().setPrettyPrinting().create()!! }
val LOCALE = Locale("en", "US")
//val BUNDLE = ResourceBundle.getBundle(
//    "isomerus",
//    LOCALE
//)!!

enum class Fonts(val font: Font) {
    SANS_SERIF(loadFont("/assets/isomerus/font/Roboto-Medium.ttf", 14.0)!!),
    MONOSPACED(loadFont("/assets/isomerus/font/FiraCode-Regular.ttf", 10.0)!!);
}
