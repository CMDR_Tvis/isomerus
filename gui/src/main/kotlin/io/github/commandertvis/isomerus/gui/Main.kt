@file:Suppress("UnusedMainParameter")

package io.github.commandertvis.isomerus.gui

import javafx.stage.Stage
import javafx.stage.StageStyle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import tornadofx.App
import tornadofx.reloadStylesheetsOnFocus

object Main : App(MainView::class, MainStyles::class) {
    init {
        reloadStylesheetsOnFocus()
    }

    @JvmStatic
    fun main(args: Array<String>) {
        GlobalScope.launch(Dispatchers.JavaFx) { start(Stage(StageStyle.DECORATED)) }
    }
}
