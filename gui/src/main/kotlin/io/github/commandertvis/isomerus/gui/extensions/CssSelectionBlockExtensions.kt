package io.github.commandertvis.isomerus.gui.extensions

import io.github.commandertvis.isomerus.gui.FONT_SMOOTHING
import io.github.commandertvis.isomerus.gui.Fonts
import tornadofx.CssSelectionBlock

fun CssSelectionBlock.setupFont(font: Fonts) {
    this.font = font.font
    fontSmoothingType = FONT_SMOOTHING
}
