plugins { application }

dependencies {
    implementation("com.jfoenix:jfoenix:8.0.8")
    implementation("com.google.code.gson:gson:2.8.5")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.1.1")
    implementation("no.tornado:tornadofx:1.7.18")
    implementation(project(":api"))
    implementation(kotlin("stdlib-jdk8"))
}

application {
    applicationName = project.parent!!.name
    mainClassName = "io.github.commandertvis.isomerus.gui.Main"
}
